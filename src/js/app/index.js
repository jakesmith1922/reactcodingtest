import React, { Component } from 'react';

class App extends Component {
  constructor() {
    super();
    // State needs to hold colours of the slot macines
    // Also needs to hold if user has won or lost
    this.state = {
      colours: [],
      hasWon: false,
    };

    this.handleSpin = this.handleSpin.bind(this);
  }

  // When Component has mounted it will run handleSpin
  componentDidMount() {
    this.handleSpin();
  }

  // handleSpin will create an array of 3 colours, generated randomly from
  // a seperate array of colour strings
  // handleSpin will then set the state colours to this new array
  // It will then call a function to check if won
  handleSpin() {
    const arrayOfColours = ['red', 'green', 'blue', 'yellow'];
    const finalArray = [];

    for (let i = 0; i < 3; i++) {
      const colour = arrayOfColours[Math.floor(Math.random() * 4)];
      finalArray.push(colour);
    }

    this.setState({
      colours: finalArray,
      hasWon: false,
    });

    this.checkIfWon(finalArray);
  }

  // To check if it has won it needs to check if all of the colour strings
  // in the array match each other. If it has 'hasWon' should be true
  checkIfWon(arr) {
    const checkItem = arr[0];
    const areColoursMatching = arr.every(colour => colour === checkItem);

    if (areColoursMatching) {
      this.setState({
        hasWon: true,
      });

      this.hasWon();
    }
  }

  hasWon() {
    const winnerText = document.querySelector('#fruit-machine');
    winnerText.innerHTML = 'WINNER!';
  }

  render() {
    const {
      colours,
    } = this.state;

    return (
      <div className='hero'>
        <div className='slot-machine'>
          <h1 id='fruit-machine'>FRUIT MACHINE</h1>
          <div className='slots'>
            {colours.map((colour, i) => {
              const key = `${ colour }${ i }`;
              return (
                <div
                  className={ `slot ${ colour }` } key={ key }
                />
              );
            })}
          </div>

          <button className='button' onClick={ this.handleSpin } />
        </div>
      </div>
    );
  }
}

export default App;
